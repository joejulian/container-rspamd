# rspamd container

rspamd is a spam filter

## Usage

A sample pod using saslauthd
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: rspamd
  labels:
    app: rspamd
spec:
  replicas: 1
  selector: 
    matchLabels
      app: rspamd
  template:
    metadata:
      labels:
        app: rspamd
    spec:
      containers:
        - name: rspamd
          image: registry.gitlab.com/joejulian/container-rspamd:latest
          imagePullPolicy: Always
          volumeMounts:
            - name: rspamd-config
              mountPath: /etc/rspamd/local.d
            - name: rspamd-dkim
            - mountPath: /keys
      volumes:
        - name: rspamd-config
          configMap:
            name: rspamd
        - name: rspamd-dkim
          secret:
            secretName: rspamd-dkim
```
